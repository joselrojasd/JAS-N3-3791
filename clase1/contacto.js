
class Agenda{

    constructor(){
        this.contactos = []
    }
    buscarContactoPorNombre(nombre){
        // para cada contacto en el arreglo 
        // de contactos de la agenda
        for (let c in this.contactos){
            if (nombre == this.contactos[c].nombre)
                return c;
        }
        return -1;
    }
    agregarContacto(nombre,telefono,tipo){
        let posicion = this.buscarContactoPorNombre(nombre)
        if (posicion==-1){
            let x = new Contacto(
                nombre,
                telefono,
                tipo
            )
            this.contactos.push(x)

            return x
        }else{
            let c=this.contactos[posicion]
            
            if (c.agregarTelefono(telefono,tipo))
                return c;
            else
                return false;
        }
    }
}

class Contacto {
    
    constructor(nombre,numero,tipo){
        this.nombre = nombre;
        this.activo = false;
        this.telefonos = []
        this.agregarTelefono(numero,tipo)
    }
    agregarTelefono(numero,tipo){
        if (!this.buscarTelefono(numero)){
            this.telefonos.push(new Telefono(numero,tipo))
            return true;
        }else
            return false;
    }
    buscarTelefono(numero){
        for (let c in this.telefonos)
            if (numero == this.telefonos[c].numero)
                return true;

        return false;
    }
}

class Telefono {

    constructor(numero,tipo){
        this.numero = numero;
        this.tipo = tipo
    }
}
