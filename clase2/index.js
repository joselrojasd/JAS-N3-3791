
let alumno = {
    "nombre":"Jose",
    "apellido":"Rojas",
    "notas":[
        80,
        95,
        30
    ],
    "fechaNac":{
        "dia":11,
        "mes":1,
        "año":1980
    },
    "conversion":""
}


function calcularEdad(fecha){
    if (fecha.año != undefined){
        let dif = (2022*365)-(fecha.año*365+fecha.mes*30+fecha.dia);
        
        return (dif/365).toFixed(0)
    }else
        return -1
}


function validarJson(texto){
    try {

        let obj = JSON.parse(texto)
        return obj;

    }catch(e){
        console.log(e)
        return null
    }
}

let conversiones = []

window.onload=()=>{

    if (localStorage.getItem("conversiones")!=undefined){
        try{
            conversiones = JSON.parse(localStorage.getItem("conversiones"));
        }catch(e){
            // por si acaso
        }
        // mostrar en el html los valores
    }
    let usuarioAutenticado = Cookies.get("usuario");

    // si la cookie no existe, es porque no ha iniciado sesion
    if (usuarioAutenticado==undefined)
        window.location="iniciarsesion.html"

    document.getElementById("usuario").innerHTML = "Bienvenido "+
                                                    usuarioAutenticado;

    document.getElementById("cerrarSesion").onclick=()=>{
        Cookies.remove('usuario')
        window.location="iniciarsesion.html"
    }

    
    let resultado = document.getElementById("resultado")
    let entrada = document.getElementById("entrada")
    document.getElementById("btnVerificar").onclick=()=>{
        objJson = validarJson(entrada.value);
        if (objJson == null){
            resultado.innerText = "Error de formato JSON"
        }else{
    
            conversiones.push(objJson)
            localStorage.setItem("conversiones",JSON.stringify(conversiones))

            alumno.conversion = objJson
            localStorage.setItem("alumno",JSON.stringify(alumno))    
    
            resultado.innerHTML = "Es valido<br>"
            let propiedades=Object.getOwnPropertyNames(objJson)
            for (p of propiedades)
                resultado.innerHTML+=p+" "+
                                    JSON.stringify(objJson[p])+
                                    "<br>"
        }
    }
    document.title = alumno.apellido
    console.log(calcularEdad({
        dia:19,
        mes:10,
        año:2001
    }))
    console.log(calcularEdad(alumno.fechaNac))
    console.log(calcularEdad("jose luis"))
}