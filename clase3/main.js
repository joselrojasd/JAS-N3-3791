
// esta asignacion elimina la referencia
// del $ a jQuery
x=4;

function agregarItem(){
    if ($("#valor").val()!=""){
        let boton="<input type='button' value='X' />"
        $("#lista").append(`<li>${$("#valor").val()} ${boton}</li>`)
        $("#valor").val("")
        $("#lista li input").click((event)=>{
            $(event.target.parentNode).remove()
        })
        if ($("#valor").hasClass("invalido"))
            $("#valor").removeClass("invalido")
    }else{
        $("#valor").addClass("invalido")
    }
}

// equivalente a window.onload
$(document).ready(()=>{

    setInterval(()=>{
        $("h1").toggleClass("azul")
    },1000)

    $("#btnAgregar").click(()=>{
        agregarItem()
    })

    $("#valor").on("keypress",(event)=>{
        if (event.keyCode == 13)
            agregarItem()
    })

    console.log(`on load...${$}`)

    $("h1").click(onclickH1)
    $("#principal").click((event)=>{
        console.log("click en el div")
        $(".p").hide()
       // $("#valor").toggleClass("invalido")
    })

})

function onclickH1(x){
    // console.log(event.target)
     console.log("click el h1 con texto: "+
                 x.target.innerText)
     $(x.target).hide()
 }